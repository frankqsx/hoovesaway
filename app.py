from flask import Flask, render_template, request, jsonify
from flask_socketio import SocketIO, send, emit
# from flask_cors import CORS
import mqtt
import random
import requests
from paho.mqtt import client as mqtt_client
import pyrebase

config = {
  "apiKey": "AIzaSyA64SnZJbvUn0hNMB98YHIdX-GljYKaukM",
  "authDomain": "m3-bd3e2.firebaseapp.com",
  "databaseURL": "https://m3-bd3e2-default-rtdb.firebaseio.com",
  "projectId": "m3-bd3e2",
  "storageBucket": "m3-bd3e2.appspot.com",
  "messagingSenderId": "919871683536",
  "appId": "1:919871683536:web:0f4e81149203268c0797d5"
}
firebase = pyrebase.initialize_app(config)

db = firebase.database()

app = Flask(__name__)
socketio = SocketIO(app)
# broker = 'broker.mqttdashboard.com'
broker = 'broker.hivemq.com'
port = 1883
topic = "hoovesaway/mqtt"
client_id = f'hoovesaway-mqtt-{random.randint(0, 1000)}'
url = "http://127.0.0.1:5000/"
# lat = 47.62
# lon = -122.17






# @app.route('/')
@app.route('/', methods=['GET', 'POST'])
def map_func():
    # if request.method == 'POST':
    #     print(request.form)
    # if request.method == 'POST':
    #     lat = request.form.get("lat")
    #     lon = request.form.get("lon")
    # print(lat)
    # print(lon)
    # else:
    loc_record = db.child("location_record").get()
    loc_dict = {}
    num = 0
    for loc in loc_record.each():
        if loc.val()["lat"] !=0:
            if int(loc.val()["time"][1])>=9:
                loc_dict[num] = loc.val()
                num += 1
    print(loc_dict)


    # if "time" in d:
    #     date = d["time"].split(" ")[0]
    #     time = d["time"].split(" ")[1]
    # else:
    #     date = "1970-01-01"
    #     time = "00:00:00"


    # print(lat,lon)
    data = loc_dict
    # data = {"lat":lat, "lon":lon, "date": date, "time":time, "dev":dev}
    # print(data)
    # return render_template('gm.html', lat = lat, lon = lon, dev = dev)
    return render_template('gm.html', data = data)
    # else:
    # return render_template('map.html')

@app.route('/date/<date>', methods = ['GET'])
def date_map_func(date):
    loc_record = db.child("location_record").get()
    zone = db.child("zone").get().val()
    loc_dict = {}
    loc_list = []
    bound_list = []
    for z in zone:
        bound_list.append(zone[z])
    num = 0
    for loc in loc_record.each():
        if loc.val()["lat"] !=0:
            if loc.val()["date"].replace("-","") == date:
                if int(loc.val()["time"][0])>=1:
            # if loc.val()["date"].split("-")[1]==date[:2] and loc.val()["date"].split("-")[2]==date[2:]:
                    loc_dict[num] = loc.val()
                    loc_list.append(loc.val())
                    num += 1
    # print(loc_dict)
    # print(loc_list)


    data = loc_list

    return render_template('gm.html', data = data, zone = zone, b = bound_list)



# @app.route('/post/', methods=['GET', 'POST'])
# def update():
#     if request.method == 'POST':
#         lat = request.form.get("lat")
#         lon = request.form.get("lon")
#         print(lat)
#         print(lon)
#         return render_template('map.html', lat = lat, lon = lon)


# def stream_handler(message):
#     print(message["event"]) # put
#     print(message["path"]) # /-K7yGTTEp7O549EzTYtI
#     print(message["data"]) # {'title': 'Pyrebase', "body": "etc..."}
#     d = db.get().val()
#     return render_template('gm.html', lat = d["lat"], lon = d["lon"])

@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')

@app.route('/index', methods=['GET'])
def index():
    return render_template('index.html')

# my_stream = db.stream(stream_handler)
# @socketio.on('my event')
# def handle_my_custom_event(json):
#     print('received json: ' + str(json))  

# @socketio.on('my event')
# def test_message(message):
#     emit('my response', {'data': message['data']}, namespace='/date/')


if __name__ == '__main__':
    socketio.run(app, debug=True)


