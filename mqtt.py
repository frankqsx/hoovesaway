import time
from datetime import timedelta, datetime
import random
import requests
from flask import json
from paho.mqtt import client as mqtt_client
import pyrebase
import json
config = {
  "apiKey": "AIzaSyA64SnZJbvUn0hNMB98YHIdX-GljYKaukM",
  "authDomain": "m3-bd3e2.firebaseapp.com",
  "databaseURL": "https://m3-bd3e2-default-rtdb.firebaseio.com",
  "projectId": "m3-bd3e2",
  "storageBucket": "m3-bd3e2.appspot.com",
  "messagingSenderId": "919871683536",
  "appId": "1:919871683536:web:0f4e81149203268c0797d5"
}

firebase = pyrebase.initialize_app(config)

db = firebase.database()


# broker = 'broker.mqttdashboard.com'
broker = 'broker.hivemq.com'
port = 1883
topic = "hoovesaway/mqtt"
client_id = f'hoovesaway-mqtt-{random.randint(0, 1000)}'
url = "http://127.0.0.1:5000/"



def is_json(myjson):
    try:
        json_object = json.loads(myjson)
    except ValueError as e:
        return False
    return True
def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    # Set Connecting Client ID
    client = mqtt_client.Client(client_id)
    # client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

def publish(client):
    msg_count = 0
    while True:
        time.sleep(1)
        msg = f"messages: {msg_count}"
        result = client.publish(topic, msg)
        # result: [0, 1]
        status = result[0]
        if status == 0:
            print(f"Send `{msg}` to topic `{topic}`")

        else:
            print(f"Failed to send message to topic {topic}")
        msg_count += 1

def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")
        json_check = is_json(msg.payload)
        # print(json_check)
        if json_check:
        # print(dict(msg))
            lat = json.loads(msg.payload.decode()).get('latitude', 0)
            lon = json.loads(msg.payload.decode()).get('longitude', 0)
            dev = json.loads(msg.payload.decode()).get('device', 'null')
            
            if isinstance(lat, str):
                if lat[-1].isalpha():
                    lat_calc = float(lat.split(" ")[0]) + float(lat.split(" ")[1])/60
                    lon_calc = float(lat.split(" ")[0]) + float(lat.split(" ")[1])/60
                    lat = lat_calc if lat.split(" ")[2] == 'N' else -lat_calc
                    lon = lon_calc if lon.split(" ")[2] == 'E' else -lon_calc

            db.child("lat").set(lat)
            db.child("lon").set(lon)
            db.child("dev").set(dev)
            # t = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) 
            t = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
            # t = datetime.strftime(datetime.now()+timedelta(days=1), "%Y-%m-%d %H:%M:%S")
            db.child("time").set(t)

            data = {"lat": lat, "lon":lon, "dev": dev, "date": t.split(" ")[0], "time": t.split(" ")[1]}
            db.child("location_record").push(data)
        # print(json.loads(msg.payload.decode()).get('latitude', 0))
        # r = requests.post(url = url, data = {'lat':lat, 'lon':lon})
        # print(r)
        # r = requests.post(url = url, data = {'lat':lat, 'lon':lon})


    client.subscribe(topic)
    client.on_message = on_message
    
def run():
    client = connect_mqtt()
    # client.loop_start()
    # publish(client)
    subscribe(client)
    client.loop_forever()



if __name__ == '__main__':
    run()