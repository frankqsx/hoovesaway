
// const firebaseConfig = {
//     apiKey: "AIzaSyA64SnZJbvUn0hNMB98YHIdX-GljYKaukM",
//     authDomain: "m3-bd3e2.firebaseapp.com",
//     databaseURL: "https://m3-bd3e2-default-rtdb.firebaseio.com",
//     projectId: "m3-bd3e2",
//     storageBucket: "m3-bd3e2.appspot.com",
//     messagingSenderId: "919871683536",
//     appId: "1:919871683536:web:0f4e81149203268c0797d5"
//   };
// firebase.initializeApp(firebaseConfig);
// const dbRef = firebase.database().ref();
  

// var db = firebase.database()
  //   var lat = {{lat}}
	// var lon = {{lon}}
  
// let lat = 47.62;
// let lon = -122.17;

// var fdbRef = firebase.database().ref('/');
// fdbRef.on('value', (snapshot) => {
// const data = snapshot.val();
//     lat = data.lat
//     lon = data.lon
// console.log(lat)

// });

// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.
let map, infoWindow, zoneFlag=false;
let zone = [
  90,
  -90,
  180,
  -180
];


var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
var icons = {
  horse: {
    icon: iconBase + 'horsebackriding.png'
    // icon: "https://icons.iconarchive.com/icons/google/noto-emoji-animals-nature/128/22227-horse-icon.png"
  },
  library: {
    icon: iconBase + 'library_maps.png'
  },
  info: {
    icon: iconBase + 'info-i_maps.png'
  }
};

var flightPlanCoordinates = [

];
var flightPath;
// function addMarker() {
//   var marker = new google.maps.Marker({
//     position: feature.position,
//     icon: icons.horse.icon,
//     map: map
//   });
// }
function addMarker(map, data, d) {
  const contentString =
    '<div id="content">' +
    '<p id="dev">'+data.dev+'</p>' +
    '<p id="time">' + data.time + " " + d+ "</p>" +
    '<p id="lat">' + data.lat.toFixed(4) + ", " + data.lon + '</p>' +
    "</div>";

  const infowindow = new google.maps.InfoWindow({
    content: contentString,
  });

  var marker = new google.maps.Marker({

    // position: { lat: 47.344, lng: -122.036 },
    position: new google.maps.LatLng(parseFloat(data.lat), parseFloat(data.lon)),
    map:map,
    // icon: "https://img.icons8.com/plasticine/100/000000/horse.png",
    // icon: "https://img.icons8.com/plumpy/24/000000/horses-sign.png",
    // icon: "https://img.icons8.com/ultraviolet/40/000000/year-of-horse.png" //blue
    // icon: "https://img.icons8.com/color/48/000000/horse-stable.png",
    // icon: icons.horse.icon,
  });
  marker.addListener("click", () => {
    infowindow.open({
      anchor: marker,
      map: map,
      shouldFocus: false,
    });
  });
  const path = flightPath.getPath();
  path.push(new google.maps.LatLng(data.lat, data.lon));
  // flightPath.insertAt();
  // flightPath.setMap(map);

}

// function initMap() {
//   map = new google.maps.Map(document.getElementById("map"), {
//     // center: { lat: parseFloat(lat), lng: parseFloat(lon) },
//     // center: new google.maps.LatLng(parseFloat(lat), parseFloat(lon)),
//     cetner: { lat: 47.6215, lng: -122.15475 },
//     zoom: 2,
//   });


//   // const contentString =
//   //   '<div id="content">' +
//   //   '<p id="dev">'+dev+'</p>' +
//   //   '<p id="time">' + time +
//   //   "</p>" +
//   //   "</div>";

//   // const infowindow = new google.maps.InfoWindow({
//   //   content: contentString,
//   // });

//   // const myLatLng = { lat: 47.6215, lng: -122.15475 };
//   // // const marker = new google.maps.Marker({
//   // //   position: myLatLng,
//   // //   map,
//   // //   title: "Uluru (Ayers Rock)",
//   // // });
//   // marker.addListener("click", () => {
//   //   infowindow.open({
//   //     anchor: marker,
//   //     map,
//   //     shouldFocus: false,
//   //   });
//   // });


//   // new google.maps.Marker({
//   //   position: myLatLng,
//   //   // icon: icons.horse.icon,
//   //   map:map,
//   //   title: "Home",
//   // });
//   const bounds = {
//     north: 47.699,
//     south: 47.59,
//     east: -122.043,
//     west: -122.249,
//   };
//   // Define a rectangle and set its editable property to true.
//   rectangle = new google.maps.Rectangle({
//     bounds: bounds,
//     editable: true,
//   });
//   // rectangle.setMap(map);
//   google.maps.event.addListener(rectangle, 'bounds_changed', function() {
//     console.log(rectangle.bounds.Eb.g);
//     console.log(rectangle.bounds.mc.i);
//   });
//   // document.getElementById("hide-poi").addEventListener("click", () => {
//   //   map.setOptions({ styles: styles["hide"] });
//   // });
//   // document.getElementById("show-poi").addEventListener("click", () => {
//   //   map.setOptions({ styles: styles["default"] });
//   // });

//   const geoFencingButton = document.createElement("button");
//   geoFencingButton.textContent = "GEO-Fencing";
//   geoFencingButton.classList.add("custom-map-control-button");
//   map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(geoFencingButton);
//   geoFencingButton.addEventListener("click", ()=> {
//     map.setOptions({ styles: styles["hide"] });
//     // document.getElementById("rect").style.display = "none"
//   });
//   const styles = {
//     default: [],
//     hide: [
//       {
//         featureType: "poi.business",
//         stylers: [{ visibility: "off" }],
//       },
//       {
//         featureType: "transit",
//         elementType: "labels.icon",
//         stylers: [{ visibility: "off" }],
//       },
//     ],
//   };
  



//   return map;
// }

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(
    browserHasGeolocation
      ? "Error: The Geolocation service failed."
      : "Error: Your browser doesn't support geolocation."
  );
  infoWindow.open(map);
}
function initMap(data) {
  const bellevue = { lat: 47.62, lng: -122.15 };
  map = new google.maps.Map(document.getElementById("map"), {
    zoom: 14,
    center: bellevue,
  });

  const lineSymbol = {
    path: google.maps.SymbolPath.CIRCLE,
    scale: 8,
    strokeColor: "#5491f5",
  };


  flightPath = new google.maps.Polyline({
    icons: [
      {
        icon: lineSymbol,
        offset: "100%",
      },
    ],

  
    // geodesic: true,
    strokeColor: "#333333",
    strokeOpacity: 0.5,
    strokeWeight: 2,
  });
  flightPath.setMap(map);
  animateCircle(flightPath);








  var lastData = data;
  // map.addListener("click", addLatLng);
  const locationButton = document.createElement("button");
locationButton.textContent = "Find my horse";
locationButton.classList.add("custom-map-control-button");
map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);
locationButton.addEventListener("click", () => {
  // Try HTML5 geolocation.
  if (navigator.geolocation ) {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        if (lastData){
          const pos = {
            lat: lastData.lat,
            lng: lastData.lon,
          };
        map.setCenter(pos);

        } else{
          const pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };
        map.setZoom(19);

        }

      },

    );
  } else {
    // Browser doesn't support Geolocation
    // handleLocationError(false, infoWindow, map.getCenter());
  }
});


}

function animateCircle(line) {
  let count = 0;
  window.setInterval(() => {
    count = (count + 1) % 200;
    const icons = line.get("icons");
    icons[0].offset = count / 2 + "%";
    line.set("icons", icons);
  }, 50);
}


window.onload = function(){
    if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        map.setCenter(pos);
      },
      () => {
        handleLocationError(true, infoWindow, map.getCenter());
      }
    );
    }
};


function inZone(lat, lng){
  if(lat<=zone[0] && lat>=zone[1] && lng<=zone[2] && lng>=zone[3]) {
    return true;
  }
  return false;
}




// Handles click events on a map, and adds a new point to the Polyline.
// function addLatLng(event) {
//   const path = flightPath.getPath();
//   // Because path is an MVCArray, we can simply append a new coordinate
//   // and it will automatically appear.
//   console.log(event.latLng);
//   path.push(new google.maps.LatLng(47.62+Math.random(), -122.15+Math.random()));
//   // Add a new marker at the new plotted point on the polyline.

// }



function safeZone(bd){
  
const geoFenceButton = document.createElement("button");
geoFenceButton.textContent = "Geo-Fencing";
geoFenceButton.classList.add("custom-map-control-button");
map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(geoFenceButton);
const rectangle = new google.maps.Rectangle({
  strokeColor: "#04aa6d",
  strokeOpacity: 0.8,
  strokeWeight: 2,
  fillColor: "#04aa6d",
  fillOpacity: 0.35,
  map,
  bounds: {
    north: bd[0],
    south: bd[1],
    east: bd[2],
    west: bd[3],
  },
  editable: true
});
zoneFlag = true;
rectangle.setMap(map);
geoFenceButton.addEventListener("click", () => {
  // Try HTML5 geolocation.
  if (!zoneFlag){
    
    if (navigator.geolocation) {
    console.log("position.coords.latitude");
    navigator.geolocation.getCurrentPosition(
      (position) => {
        zoneFlag = true;
        var zoneLength = 0.0002;
        // var marker = new google.maps.Marker({

        //   // position: { lat: 47.344, lng: -122.036 },
        //   position: new google.maps.LatLng(parseFloat(data.lat), parseFloat(data.lon)),
        //   map:map,
        //   // icon: "https://img.icons8.com/plasticine/100/000000/horse.png",
        //   // icon: "https://img.icons8.com/plumpy/24/000000/horses-sign.png",
        //   // icon: "https://img.icons8.com/ultraviolet/40/000000/year-of-horse.png" //blue
        //   // icon: "https://img.icons8.com/color/48/000000/horse-stable.png",
        //   icon: "http://maps.google.com/mapfiles/kml/shapes/arrow.png",
        // });


        // const zoneCoords = [
        //   { lat: position.coords.latitude+zoneLength, lng: position.coords.longitude+zoneLength },
        //   { lat: position.coords.latitude+zoneLength, lng: position.coords.longitude-zoneLength },
        //   { lat: position.coords.latitude-zoneLength, lng: position.coords.longitude-zoneLength },
        //   { lat: position.coords.latitude-zoneLength, lng: position.coords.longitude+zoneLength },

        // ];
        // const bermudaTriangle = new google.maps.Polygon({
        //   paths: zoneCoords,
        //   strokeColor: "#FF0000",
        //   strokeOpacity: 0.8,
        //   strokeWeight: 2,
        //   fillColor: "#FF0000",
        //   fillOpacity: 0.35,
        // });
        // bermudaTriangle.setMap(map);
        // var bounds = {
        //   north: position.coords.latitude+zoneLength,
        //   south: position.coords.latitude-zoneLength,
        //   east: position.coords.longitude+zoneLength,
        //   west: position.coords.longitude-zoneLength
        // };
        // rectangle.setBounds(bounds);
        rectangle.setMap(map);
        google.maps.event.addListener(rectangle, 'bounds_changed', function() {
          const ne = rectangle.getBounds().getNorthEast();
          const sw = rectangle.getBounds().getSouthWest();
          const contentString =
          "<b>Rectangle moved.</b><br>" +
          "New north-east corner: " +
          ne.lat() +
          ", " +
          ne.lng() +
          "<br>" +
          "New south-west corner: " +
          sw.lat() +
          ", " +
          sw.lng();
          console.log(contentString);
          zone = [ne.lat(), ne.lng(), sw.lat(), sw.lng()];
          console.log(inZone(position.coords.latitude, position.coords.longitude));
        });        
      },

    );}
  } else {
    rectangle.setMap(null);
    zoneFlag = false;
    // Browser doesn't support Geolocation
    // handleLocationError(false, infoWindow, map.getCenter());
  }
});
}